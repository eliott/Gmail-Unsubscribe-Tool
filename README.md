# Désabonnement Automatique des Newsletters Gmail

Ce script Python automatise le processus de désabonnement des newsletters dans votre boîte de réception Gmail. Il parcourt vos e-mails, identifie les liens de désabonnement et tente de vous désinscrire automatiquement des newsletters indésirables.

## Fonctionnalités

- Connexion sécurisée à Gmail via IMAP
- Analyse des e-mails pour trouver les liens de désabonnement
- Tentative automatique de désabonnement
- Gestion des erreurs et des tentatives multiples
- Rapport sur les désabonnements réussis et échoués

## Prérequis

- Python 3.x
- Accès IMAP activé sur votre compte Gmail
- Les bibliothèques Python suivantes :
  - imaplib
  - email
  - requests
  - beautifulsoup4
  - urllib3

## Installation

1. Clonez ce dépôt ou téléchargez le script.
2. Installez les dépendances :

```pip install requests beautifulsoup4 urllib3```

## Configuration

1. Ouvrez le script dans un éditeur de texte.
2. Remplacez `email_address` et `password` par vos identifiants Gmail :
```
email_address = "votre_email@gmail.com"
password = "votre_mot_de_passe_ou_mot_de_passe_d_application"
```

Note : Si vous utilisez l'authentification à deux facteurs, vous devrez générer un mot de passe d'application spécifique pour ce script.

Exécutez le script avec Python :

```python nom_du_script.py```

Le script parcourra votre boîte de réception, tentera de se désabonner des newsletters et affichera un résumé des actions effectuées.