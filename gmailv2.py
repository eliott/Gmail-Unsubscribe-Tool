import imaplib
import email
import re
import requests
from bs4 import BeautifulSoup
import time
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry

def connect_to_gmail(email_address, password):
    imap_server = "imap.gmail.com"
    imap_port = 993
    
    imap_client = imaplib.IMAP4_SSL(imap_server, imap_port)
    imap_client.login(email_address, password)
    return imap_client

def find_unsubscribe_link(email_body):
    soup = BeautifulSoup(email_body, 'html.parser')
    unsubscribe_links = soup.find_all('a', href=re.compile(r'unsubscribe|opt-out', re.I))
    
    if unsubscribe_links:
        return unsubscribe_links[0]['href']
    return None

def requests_retry_session(
    retries=3,
    backoff_factor=0.3,
    status_forcelist=(500, 502, 504),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session

def unsubscribe(imap_client, email_address, password):
    imap_client.select('INBOX')
    _, message_numbers = imap_client.search(None, 'ALL')
    
    successful_unsubscribes = 0
    failed_unsubscribes = 0
    
    for num in message_numbers[0].split():
        _, msg_data = imap_client.fetch(num, '(RFC822)')
        email_body = msg_data[0][1]
        email_message = email.message_from_bytes(email_body)
        
        if email_message.is_multipart():
            for part in email_message.walk():
                if part.get_content_type() == "text/html":
                    try:
                        body = part.get_payload(decode=True).decode('utf-8')
                    except UnicodeDecodeError:
                        try:
                            body = part.get_payload(decode=True).decode('iso-8859-1')
                        except UnicodeDecodeError:
                            print(f"Impossible de décoder l'e-mail {num}")
                            failed_unsubscribes += 1
                            continue
                    
                    unsubscribe_link = find_unsubscribe_link(body)
                    
                    if unsubscribe_link:
                        print(f"Lien de désabonnement trouvé: {unsubscribe_link}")
                        try:
                            response = requests_retry_session().get(unsubscribe_link, timeout=10)
                            if response.status_code == 200:
                                print("Désabonnement réussi!")
                                successful_unsubscribes += 1
                            else:
                                print(f"Échec du désabonnement. Code de statut: {response.status_code}")
                                failed_unsubscribes += 1
                        except Exception as e:
                            print(f"Erreur lors du désabonnement: {str(e)}")
                            failed_unsubscribes += 1
                    break

    imap_client.close()
    imap_client.logout()
    
    return successful_unsubscribes, failed_unsubscribes

if __name__ == "__main__":
    email_address = "à remplir"
    password = "à remplir"
    
    imap_client = connect_to_gmail(email_address, password)
    successes, failures = unsubscribe(imap_client, email_address, password)
    
    print(f"\nRésumé:")
    print(f"Désabonnements réussis: {successes}")
    print(f"Désabonnements échoués: {failures}")